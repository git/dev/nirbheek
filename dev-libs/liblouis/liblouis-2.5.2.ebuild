# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

inherit eutils

DESCRIPTION="A Braille translation and back-translation library"
SRC_URI="http://${PN}.googlecode.com/files/${P}.tar.gz"
HOMEPAGE="http://code.google.com/p/liblouis/"

LICENSE="GPL-3 LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+wide-unicode"

RDEPEND=""
DEPEND="${RDEPEND}
	sys-apps/help2man
	virtual/pkgconfig"

src_prepare() {
	DOCS="AUTHORS ChangeLog NEWS README TODO"
	default
}

src_configure() {
	econf --disable-static \
		$(use_enable wide-unicode ucs4)
}

src_install() {
	default
	prune_libtool_files
}
