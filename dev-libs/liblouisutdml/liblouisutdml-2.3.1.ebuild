# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

inherit eutils

DESCRIPTION="Adds Unified Tactile Document Markup Language support to liblouis"
SRC_URI="http://${PN}.googlecode.com/files/${P}.tar.gz"
HOMEPAGE="http://code.google.com/p/liblouis/"

LICENSE="GPL-3 LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-libs/libxml2:2
	dev-libs/liblouis"
DEPEND="${RDEPEND}
	sys-apps/help2man
	virtual/pkgconfig"

src_prepare() {
	DOCS="AUTHORS ChangeLog NEWS README"
	default
}

src_configure() {
	econf --disable-static
}

src_install() {
	default
	prune_libtool_files
}
