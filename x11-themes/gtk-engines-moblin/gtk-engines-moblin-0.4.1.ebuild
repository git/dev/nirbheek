# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit autotools

MY_PN="moblin-gtk-engine"

DESCRIPTION="Mobline GTK+ Cairo Engine"
HOMEPAGE="http://www.moblin.org/"
SRC_URI="http://git.moblin.org/cgit.cgi/${MY_PN}/snapshot/${MY_PN}-${PV}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=x11-libs/gtk+-2.8.0"
DEPEND="${RDEPEND}
dev-util/pkgconfig"

S="${WORKDIR}/${MY_PN}-${PV}"

src_prepare() {
	eautoreconf
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"

	dodoc AUTHORS ChangeLog README NEWS
}
