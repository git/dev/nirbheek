# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-i18n/ibus-anthy/ibus-anthy-1.5.0.ebuild,v 1.3 2013/04/29 12:13:35 naota Exp $

EAPI="5"
VALA_USE_DEPEND="vapigen"
VALA_MIN_API_VERSION="0.14"
inherit eutils vala

DESCRIPTION="XCompose input method for the IBus Framework"
HOMEPAGE="http://code.google.com/p/ibus/"
SRC_URI="http://ibus.googlecode.com/files/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+dconf gnome +gtk3 +introspection +vala"

RDEPEND="
	app-text/iso-codes
	x11-libs/libxkbfile

	>=dev-libs/glib-2.26:2
	>=app-i18n/ibus-1.5.0[vala?]

	dconf? ( >=gnome-base/dconf-0.7.5 )
	gnome? ( gnome-base/libgnomekbd )
	gtk3? ( x11-libs/gtk+:3 )
	introspection? ( >=dev-libs/gobject-introspection-0.6.8 )"
DEPEND="${RDEPEND}
	>=dev-util/intltool-0.35
	sys-devel/gettext
	virtual/pkgconfig
	vala? ( $(vala_depend) )"

REQUIRED_USE="
	gtk3? ( vala )
	vala? ( introspection )"

src_prepare() {
	default
	vala_src_prepare
}

src_configure() {
	# TODO: use_enable python
	econf --disable-maintainer-mode \
		--disable-gconf \
		--disable-python \
		--enable-xkb \
		$(use_enable dconf) \
		$(use_enable gtk3) \
		$(use_enable vala)
}

src_install() {
	default
	prune_libtool_files
}
