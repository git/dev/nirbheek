# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit eutils games

DESCRIPTION="A first-person survival-horror adventure game"
HOMEPAGE="http://www.penumbra-overture.com/"
MY_PN="PenumbraOverture"
MY_P="${P/-bin/}"
SRC_URI="${MY_P//-/_}.sh"

LICENSE="PENUMBRA-EULA"
SLOT="0"
KEYWORDS="-* ~amd64 ~x86"
IUSE="alut fltk +ogg +openal +sdl +system-libs"
RESTRICT="fetch strip"

DEPEND="app-arch/xz-utils"
RDEPEND="sys-libs/glibc
	x11-libs/libX11

	system-libs? (
		media-libs/mesa

		alut? ( >=media-libs/freealut-1.1.0 )
		fltk? ( x11-libs/fltk:1.1 )
		openal? ( >=media-libs/openal-1.8.466 )
		sdl? (
			>=media-libs/libsdl-1.2.11
			>=media-libs/sdl-image-1.2.5
			>=media-libs/sdl-ttf-2.0.8
		)
		ogg? (
			>=media-libs/libvorbis-1.1.0
			>=media-libs/libogg-1.1.2
		)
	)"

S="${WORKDIR}/${MY_PN}"

src_unpack() {
	cd "${WORKDIR}"

	# makeself gzip archive
	local a="${DISTDIR}/${A}"
	# makeself archive script offset
	local offset=$(head -n 439 "$a" | wc -c | tr -d " ")

	einfo "The game is a tar.lzma inside a tar.lzma inside a makeself archive."
	einfo "This will take some time (and RAM) to extract. Go get some coffee."

	ebegin "Extracting ${MY_PN}"
	dd if=$a ibs=$offset skip=1 obs=1024 2>/dev/null | tar xz ./subarch -O | \
		lzmadec | tar x instarchive_all -O | lzmadec | tar x || \
		die "Extracting archive failed!"
	eend
}

src_install() {
	local dir=${GAMES_PREFIX_OPT}/${MY_PN}

	if use system-libs; then
		ebegin "Removing bundled libraries"

		# Use system GLU, aka mesa
		rm ./lib/libGLU* || die "Removing GLU failed"

		use alut && { rm ./lib/libalut* || die "Removing alut failed"; }
		use fltk && { rm ./lib/libfltk* || die "Removing fltk failed"; }
		use ogg && { rm ./lib/lib{ogg,vorbis}* || die "Removing ogg failed"; }
		use openal && { rm ./lib/libopenal* || die "Removing openal failed"; }
		use sdl && { rm ./lib/libSDL* || die "Removing SDL failed"; }

		eend
	fi

	insinto "${GAMES_PREFIX_OPT}"
	doins -r "${S}" || die "doins failed"

	dodoc *.txt README.* Manual.pdf || die "dodoc failed"

	exeinto "${dir}"
	doexe penumbra || die "doexe failed"
	doexe penumbra.bin || die "doexe failed"

	games_make_wrapper penumbra-bin ./penumbra "${dir}" "${dir}"

	mv penumbra.png penumbra-bin.png || die "Rename png failed"
	doicon penumbra-bin.png || die "doicon failed"
	make_desktop_entry penumbra-bin "Penumbra: Overture" penumbra-bin

	prepgamesdirs
}

pkg_postinst() {
	if use system-libs; then
		elog "We remove the bundled penumbra libs in favour of system libraries."
		elog "If you face problems with penumbra, try USE=-system-libs"
	fi
}
