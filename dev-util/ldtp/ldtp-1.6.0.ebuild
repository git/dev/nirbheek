# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"

inherit versionator

MY_PV="$(get_version_component_range 1-2)"
DESCRIPTION="A high quality test automation framework for desktop applications"
HOMEPAGE="http://ldtp.freedesktop.org"
SRC_URI="http://download.freedesktop.org/${PN}/${MY_PV%%.*}.x/${MY_PV}.x/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="localization +python"
DEPEND="gnome-extra/at-spi
	    >=dev-libs/glib-2.2.0
		gnome-extra/libgail-gnome
		gnome-base/gail
		python? ( virtual/python )
		localization? ( sys-devel/gettext )
	    dev-libs/libxml2"
RDEPEND="${DEPEND}"


src_compile() {
	econf \
		--enable-goptionparse \
		--enable-newroles \
		$(use_enable localization) \
		$(use_with python pythonmodules)

	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "install failed"
	dodoc AUTHORS NEWS README TODO
}
